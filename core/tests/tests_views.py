"""
File for view testing
"""
import unittest

from django.test import TestCase
from django.core.urlresolvers import resolve
from django.contrib.auth.models import User
from django.http import HttpRequest
from unittest.mock import patch, Mock
from core import views, queries
from core.models import Greeting, JourneyNodes
 

class ViewsTest(TestCase):
    """
    View Test Class
    """

    @patch.object(views,'render')
    def test_render_journey(self,mock_render):
        """
        Test case of rendering of journey function for non-registered
        user with request GET
        """
        request = HttpRequest()
        request.method = "GET"
        request.user = Mock()  
        request.user.is_anonymous().return_value = True
        mock_render.return_value = "correct answer"
        self.assertEqual(views.journey(request),"correct answer")
        mock_render.assert_called_with(request, 'journey.html', {'position': 'node0'})

    @patch.object(views,'render')
    def test_post_journey_with_anonymous_user(self, mock_render):
        """
        Test case of method POST in journey function
        where user is not registered and his node position will 
        not get updated
        """
        request = HttpRequest()
        request.method = "POST"
        request.POST = Mock()
        request.user = Mock()
        request.user.is_anonymous().return_value = True
        mock_render.return_value = "correct answer"
        self.assertEqual(views.journey(request),"correct answer")
        mock_render.assert_called_with(request, 'journey.html', {'position': 'node0'})

    @patch.object(queries,'update_go_user')
    def test_post_journey_and_update_go_user_is_true(self, mock_update_go_user):
        """
        Test case of method POST in journey function
        where update_go_user was sucessful
        """
        request = HttpRequest()
        request.method = "POST"
        request.POST = Mock()
        request.user = Mock()
        request.user.is_anonymous().return_value = True
        mock_update_go_user.return_value = True
        self.assertEqual(queries.update_go_user(request.user, request.user.get("id")), True)
        mock_update_go_user.assert_called_with(request.user, request.user.get("id"))

    @patch.object(queries,'update_go_user')
    def test_post_journey_and_update_go_user_is_false(self, mock_update_go_user):
        """
        Test case of method POST in journey function
        where update_go_user was unsucessful
        """
        request = HttpRequest()
        request.method = "POST"
        request.POST = Mock()
        request.user = Mock()
        request.user.is_anonymous().return_value = False
        mock_update_go_user.return_value = False
        self.assertEqual(queries.update_go_user(request.user, request.user.POST.get("id")), False)
        mock_update_go_user.assert_called_with(request.user, request.user.POST.get("id"))

    @patch.object(views,'render')
    @patch.object(views,'get_level')
    def test_render_journey_with_get_level(self,mock_get_level,mock_render):
        """
        Test case of method render in journey function
        with getting position
        """
        request = HttpRequest()
        request.method = "GET"
        request.user = Mock()
        request.user.is_anonymous.return_value = False
        mock_get_level.side_effect = "1"
        views.journey(request)
        mock_render.assert_called_with(request, 'journey.html', {'position': 'node1'})
        mock_get_level.assert_called_with(request.user)

    def test_journey(self):
        """
        Test the correct journey view is loaded with its URL
        """
        resolver = resolve('/journey/')
        self.assertEqual(resolver.view_name, 'journey')

    def test_index(self):
        """
        Test the correct index view is loaded with its URL
        """
        resolver = resolve('/')
        self.assertEqual(resolver.view_name, 'index')

    @patch.object(views,'render')
    def test_render_index(self,mock_render):
        """
        Test case of rendering of index
        """
        request = HttpRequest()
        request.method = "GET"
        mock_render.return_value = "correct answer"
        self.assertEqual(views.index(request),"correct answer")
        mock_render.assert_called_with(request, 'index.html')

    def test_settings(self):
        """
        Test the correct settings view is loaded with its URL
        """
        resolver = resolve('/account/settings/')
        self.assertEqual(resolver.view_name, 'settings')

    @patch.object(views,'render')
    def test_render_settings(self,mock_render):
        """
        Test case of rendering of settings
        """
        request = HttpRequest()
        request.method = "GET"
        mock_render.return_value = "correct answer"
        self.assertEqual(views.settings(request),"correct answer")
        mock_render.assert_called_with(request, 'settings.html')

    def test_profile(self):
        """
        Test the correct profile view is loaded with its URL
        """
        resolver = resolve('/account/profile/')
        self.assertEqual(resolver.view_name, 'profile')

    @patch.object(views,'render')
    @patch.object(views,'get_level')
    def test_render_profile(self,mock_get_level,mock_render):
        """
        Test case of rendering of profile
        """
        request = HttpRequest()
        request.method = "GET"
        mock_render.return_value = "correct answer"
        request.user = Mock()
        mock_get_level.side_effect = "1"
        nodes = JourneyNodes.objects.all()
        self.assertEqual(views.profile(request),"correct answer")

    def test_visits(self):
        """
        Test the correct visit view is loaded with its URL
        """
        resolver = resolve('/visits')
        self.assertEqual(resolver.view_name, 'visits')

    @patch.object(views,'render')
    def test_render_visits(self,mock_render):
        """
        Test case of rendering of visits
        """
        request = HttpRequest()
        request.method = "GET"
        mock_render.return_value = "correct answer"
        self.assertEqual(views.visits(request),"correct answer")

    def test_tutorial(self):
        """
        Test the correct tutorial view is loaded with its URL
        """
        for num in range(1,7):
            resolver = resolve('/journey/tutorial/'+str(num))
            self.assertEqual(resolver.view_name, 'tutorial')

    @patch.object(views,'render')
    def test_render_tutorial(self,mock_render):
        """
        Test case of rendering of tutoriasl 1 to 7
        """
        request = HttpRequest()
        request.method = "GET"
        mock_render.return_value = "correct answer"
        for num in range(1,7):
            test_num = str(num)
            self.assertEqual(views.tutorial(request, test_num),"correct answer")
            mock_render.assert_called_with(request, 'tutorial/tutorial_'+test_num+'.html')
