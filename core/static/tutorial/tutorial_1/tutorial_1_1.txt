As you saw, each time you click on the board, you send one of your soldiers down!
<br>
No special snowflakes in this army. Each soldier has the same function and
this is important to remember: they can't move once you set them down!
<br>
...
<br>
Yes. They're very loyal and will defend that area to the end of their life.
They are also afraid of open spaces and will only stick to the intersections between lines.
<br>
...
<br>
Don't question them! Just be happy you're not part of them!
<br>
<br>
Okay, back to teaching! You should see four soldiers already deployed.
Deploy one more solider to the center to essentially claim all the things!
